## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
A calculator for words

### Short Description
Valid commands in sentences are: sum(+) is "sum", minus(-) is "minus", times(*) is "times", divide(/) is "divided by".

Valid user input numbers are: 1 is "one", 2 is "two", 3 is "three", 4 is "four", 5 is "five".

Sum exapmle: "four sum three" = "Result is 7".

Minus example: "four minus three" = "Result is 1".

Times example: "four times three" = "Result is 12".

Divide example: " four divided by three" = "Result is 1.33333".

Restrictions = You cant count numbers with same numbers like "four sum four". You can only minus and divide higher number with lower number. You can only use two numbers also.

### Install
Make sure you have gcc version 11.1.0  
g++ main.cpp -o main and cmake

### Usage
$ mkdir build && cd build  
$ cmake ../  
$ make  
$ ./main

### Maintainer(s)

Kalle Nurminen @kalle.nurminen98

### Contributing

Kalle Nurminen @kalle.nurminen98

### License

Free to use. No license
