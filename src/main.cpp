#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
int main()
{
    // Definitons
    std::string sentence;
    std::string sum = "sum";
    std::string minus = "minus";
    std::string times = "times";
    std::string divide = "dividedby";
    std::string one = "one";
    std::string two = "two";
    std::string three = "three";
    std::string four = "four";
    std::string five = "five";
    std::string::size_type int_type;
    // User input
    std::cout << "Write a sentence: " << std::endl;
    std::getline(std::cin, sentence);
    // Removing spaces
    sentence.erase(std::remove(sentence.begin(), sentence.end(), ' '), sentence.end());

    // If sentence have word sum
    if(sentence.find(sum) != std::string::npos)
    {
        // Changing string to int
        int value;
        std::istringstream(one) >> value;
        std::istringstream(two) >> value;
        std::istringstream(three) >> value;
        std::istringstream(four) >> value;
        std::istringstream(five) >> value;
        // If statements if sentence have that word
        if(sentence.find(one) != std::string::npos)
        {
            value += 1;
        }
        if(sentence.find(two) != std::string::npos)
        {
            value += 2;
        }
        if(sentence.find(three) != std::string::npos)
        {
            value += 3;
        }
        if(sentence.find(four) != std::string::npos)
        {
            value += 4;
        }
        if(sentence.find(five) != std::string::npos)
        {
            value += 5;
        }
        int numbers_sum = value;
        std::cout << "Result is " << numbers_sum << std::endl;
    }
    // If sentence have word sum
    else if(sentence.find(minus) != std::string::npos)
    {
        int value1;
        int value2;
        int value3;
        int value4;
        int value5;
        int numbers;
        // Changing string to int
        std::istringstream(one) >> value1;
        std::istringstream(two) >> value2;
        std::istringstream(three) >> value3;
        std::istringstream(four) >> value4;
        std::istringstream(five) >> value5;
        // If statements if sentence have that word
        if(sentence.find(one) != std::string::npos)
        {
            value1 = 1;

        }
        if(sentence.find(two) != std::string::npos)
        {
            value2 = 2;
            numbers = value2- value1;

        }
        if(sentence.find(three) != std::string::npos)
        {
            value3 = 3;
            numbers = value3 -value2- value1;

        }
        if(sentence.find(four) != std::string::npos)
        {
            value4 = 4;
            numbers = value4 -value3 -value2- value1;
 
        }
        if(sentence.find(five) != std::string::npos)
        {
            value5 = 5;
            numbers = value5 - value4 -value3 -value2- value1;
        }

        std::cout << "Result is " << numbers << std::endl;
    }
    // If sentence have word sum
    else if(sentence.find(times) != std::string::npos)
    {
        int value;
        int numbers;
        // Changing string to int
        std::istringstream(one) >> value;
        std::istringstream(two) >> value;
        std::istringstream(three) >> value;
        std::istringstream(four) >> value;
        std::istringstream(five) >> value;
        value = 1;
        // If statements if sentence have that word
        if(sentence.find(one) != std::string::npos)
        {
            value *= 1;
        }
        if(sentence.find(two) != std::string::npos)
        {
            value *= 2;
        }
        if(sentence.find(three) != std::string::npos)
        {
            value *= 3;
        }
        if(sentence.find(four) != std::string::npos)
        {
            value *= 4;
        }
        if(sentence.find(five) != std::string::npos)
        {
            value *= 5;
        }
        numbers = value;
        std::cout << "Result is " << numbers << std::endl;
    }
    // If sentence have word sum
    else if(sentence.find(divide) != std::string::npos)
    {
        double value1;
        double value2;
        double value3;
        double value4;
        double value5;
        double numbers;
        // Changing string to int
        std::istringstream(one) >> value1;
        std::istringstream(two) >> value2;
        std::istringstream(three) >> value3;
        std::istringstream(four) >> value4;
        std::istringstream(five) >> value5;
        // If statements if sentence have that word
        if(sentence.find(one) != std::string::npos)
        {
            value1 = 1;
        }
        if(sentence.find(two) != std::string::npos)
        {
            value2 = 2;
        }
        if(sentence.find(three) != std::string::npos)
        {
            value3 = 3;
        }
        if(sentence.find(four) != std::string::npos)
        {
            value4 = 4;
        }
        if(sentence.find(five) != std::string::npos)
        {
            value5 = 5;
        }
        // If statements to dividing higher number with lower number
        if(value2==2 && value1==1)
        {
            numbers = value2 / value1;
        }
        if(value3==3 && value1==1)
        {
            numbers = value3 / value1;
        }
        if(value3==3 && value2==2)
        {
            numbers = value3 / value2;
        }
        if(value4==4 && value1==1)
        {
            numbers = value4 / value1;
        }
        if(value4==4 && value2==2)
        {
            numbers = value4 / value2;
        }
        if(value4==4 && value3==3)
        {
            numbers = value4 / value3;
        }
        if(value5==5 && value1==1)
        {
            numbers = value5 / value1;
        }
        if(value5==5 && value2==2)
        {
            numbers = value5 / value2;
        }
        if(value5==5 && value3==3)
        {
            numbers = value5 / value3;
        }
        if(value5==5 && value4==4)
        {
            numbers = value5 / value4;
        }

        std::cout << "Result is " << numbers << std::endl;
    }
    return 0;
}